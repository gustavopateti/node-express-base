// =============================================================================
// index.js
// NodeJS app entry point
// Sets up and starts the server express app
// =============================================================================
import appPromise from './src/server'

// selects port depending on environment
const isProduction = process.env.NODE_ENV === 'production'
const port = isProduction ? process.env.PORT : 3001

// waits for the express app and starts the server
async function startServer () {
  let app
  try {
    app = await appPromise()
    app.listen(port, err => {
      if (err) {
        console.error(err)
      } else {
        console.log(`Server running on port ${port}`)
      }
    })
  } catch (err) {
    console.error(err)
  }
}

startServer()
