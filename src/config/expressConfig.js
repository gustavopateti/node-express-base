// =============================================================================
// expressConfig.js
// Middleware set up and configuration of the express app
// =============================================================================
import methodOverride from 'method-override'
import bodyParser from 'body-parser'
import cors from 'cors'
import morgan from 'morgan'
import compression from 'compression'

export default function (app) {
  // enable use of PUT and other requests trough name overriding
  app.use(methodOverride())

  // parse application/x-www-form-urlencoded
  app.use(bodyParser.urlencoded({ extended: 'true' }))
  // parse application/json
  app.use(bodyParser.json())
  // parse application/vnd.api+json as json
  app.use(bodyParser.json({ type: 'application/vnd.api+json' }))

  // enable cross-origin resource sharing
  app.use(cors())

  // enable server logger
  app.use(morgan('dev'))

  // enable response compression for improved performance
  app.use(compression())

  // test route
  app.get('/', function (req, res) {
    res.status(200).send('<h1>Hello, welcome to my app!</h1>')
  })
}
