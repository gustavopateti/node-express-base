// =============================================================================
// server.js
// Server express app setup and configuration
// =============================================================================
import express from 'express'
import expressConfig from './config/expressConfig'

// create express app
const app = express()

// array of config functions
const configs = [expressConfig]

// mapping of every config function on the express app
const setup = configs.map(config => config(app))

// awaits for the setup functions to finish then return the app
async function appPromise () {
  try {
    await Promise.all(setup)
    return app
  } catch (err) {
    return err
  }
}

// exports the app promise
export default appPromise
