'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

exports.default = function (app) {
  // enable use of PUT and other requests trough name overriding
  app.use((0, _methodOverride2.default)());

  // parse application/x-www-form-urlencoded
  app.use(_bodyParser2.default.urlencoded({ extended: 'true' }));
  // parse application/json
  app.use(_bodyParser2.default.json());
  // parse application/vnd.api+json as json
  app.use(_bodyParser2.default.json({ type: 'application/vnd.api+json' }));

  // enable cross-origin resource sharing
  app.use((0, _cors2.default)());

  // enable server logger
  app.use((0, _morgan2.default)('dev'));

  // enable response compression for improved performance
  app.use((0, _compression2.default)());

  // test route
  app.get('/', function (req, res) {
    res.status(200).send('<h1>Hello, welcome to my app!</h1>');
  });
};

var _methodOverride = require('method-override');

var _methodOverride2 = _interopRequireDefault(_methodOverride);

var _bodyParser = require('body-parser');

var _bodyParser2 = _interopRequireDefault(_bodyParser);

var _cors = require('cors');

var _cors2 = _interopRequireDefault(_cors);

var _morgan = require('morgan');

var _morgan2 = _interopRequireDefault(_morgan);

var _compression = require('compression');

var _compression2 = _interopRequireDefault(_compression);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }