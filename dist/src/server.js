'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _regenerator = require('babel-runtime/regenerator');

var _regenerator2 = _interopRequireDefault(_regenerator);

var _promise = require('babel-runtime/core-js/promise');

var _promise2 = _interopRequireDefault(_promise);

var _asyncToGenerator2 = require('babel-runtime/helpers/asyncToGenerator');

var _asyncToGenerator3 = _interopRequireDefault(_asyncToGenerator2);

// awaits for the setup functions to finish then return the app
var appPromise = function () {
  var _ref = (0, _asyncToGenerator3.default)( /*#__PURE__*/_regenerator2.default.mark(function _callee() {
    return _regenerator2.default.wrap(function _callee$(_context) {
      while (1) {
        switch (_context.prev = _context.next) {
          case 0:
            _context.prev = 0;
            _context.next = 3;
            return _promise2.default.all(setup);

          case 3:
            return _context.abrupt('return', app);

          case 6:
            _context.prev = 6;
            _context.t0 = _context['catch'](0);
            return _context.abrupt('return', _context.t0);

          case 9:
          case 'end':
            return _context.stop();
        }
      }
    }, _callee, this, [[0, 6]]);
  }));

  return function appPromise() {
    return _ref.apply(this, arguments);
  };
}();

// exports the app promise


var _express = require('express');

var _express2 = _interopRequireDefault(_express);

var _expressConfig = require('./config/expressConfig');

var _expressConfig2 = _interopRequireDefault(_expressConfig);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

// create express app
// =============================================================================
// server.js
// Server express app setup and configuration
// =============================================================================
var app = (0, _express2.default)();

// array of config functions
var configs = [_expressConfig2.default];

// mapping of every config function on the express app
var setup = configs.map(function (config) {
  return config(app);
});exports.default = appPromise;