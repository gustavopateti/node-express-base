'use strict';

var _regenerator = require('babel-runtime/regenerator');

var _regenerator2 = _interopRequireDefault(_regenerator);

var _asyncToGenerator2 = require('babel-runtime/helpers/asyncToGenerator');

var _asyncToGenerator3 = _interopRequireDefault(_asyncToGenerator2);

// waits for the express app and starts the server
var startServer = function () {
  var _ref = (0, _asyncToGenerator3.default)( /*#__PURE__*/_regenerator2.default.mark(function _callee() {
    var app;
    return _regenerator2.default.wrap(function _callee$(_context) {
      while (1) {
        switch (_context.prev = _context.next) {
          case 0:
            app = void 0;
            _context.prev = 1;
            _context.next = 4;
            return (0, _server2.default)();

          case 4:
            app = _context.sent;

            app.listen(port, function (err) {
              if (err) {
                console.error(err);
              } else {
                console.log('Server running on port ' + port);
              }
            });
            _context.next = 11;
            break;

          case 8:
            _context.prev = 8;
            _context.t0 = _context['catch'](1);

            console.error(_context.t0);

          case 11:
          case 'end':
            return _context.stop();
        }
      }
    }, _callee, this, [[1, 8]]);
  }));

  return function startServer() {
    return _ref.apply(this, arguments);
  };
}();

var _server = require('./src/server');

var _server2 = _interopRequireDefault(_server);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

// selects port depending on environment
var isProduction = process.env.NODE_ENV === 'production'; // =============================================================================
// index.js
// NodeJS app entry point
// Sets up and starts the server express app
// =============================================================================

var port = isProduction ? process.env.PORT : 3001;

startServer();